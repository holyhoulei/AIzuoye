# 第二周作业：第一章~第二章：计算机视觉处理算法基础及视觉特征提取

1. 画图解释图像卷积滤波的基本原理，并进一步简述常见的图像平滑滤波算法。 
    为什么要滤波：为了消除和减弱图像中不必要的噪声。
    卷积滤波的基本原理：卷积核与图像对应位置的乘积和，卷积操作在做乘积之前，需要先将卷积核翻转180度，之后再做乘积。
    ![卷积滤波的基本原理](_v_images/20190811222146241_32083.png)
    
    常见的图像平滑滤波算法：
    平滑滤波：
        平均滤波：线型滤波，在小的区域内（通常是3 * 3）的像素值平均，通常有4临域、8临域滤波器
        加权平均滤波（高斯滤波）：线型滤波，在平均滤波的基础上，对每个像素增加了系数，权值选取主要有多种方式，其中符合高斯模板的，是高斯滤波。
        中值滤波：将窗口内像素按灰度值大小排序，取中间值代替原窗口像素值（因此滤波器大小像素数需要时奇数），窗口的不同决定了不同的中值滤波方式。滤波器通常有（3* 3、5*5），对椒盐噪声有效。
    数学形态学滤波：
         膨胀：膨胀(dilate)就是求局部的最大值的操作。从数学的角度就是图像与核进行卷积，核可以是任何形状核大小，它拥有一个单独定义出来的参考点，称为锚点，可以把核视为模板或者掩码 
         腐蚀：腐蚀就是求局部最小值的操作 ，和膨胀并不是互为逆运算的关系
    参考文档：
    [图像处理中滤波（filtering）与卷积（convolution）的区别](https://blog.csdn.net/haoji007/article/details/53911940)
    [数字图像处理中滤波和卷积操作详细说明](https://www.cnblogs.com/xiaojianliu/p/9075872.html)
    [图像卷积与滤波的一些知识点](https://blog.csdn.net/zouxy09/article/details/49080029)
    [Opencv3笔记11——形态学滤波（1）](https://blog.csdn.net/huayunhualuo/article/details/81486884)
    
2. 简述边缘检测的基本原理，以及Sobel、LoG和Canny算子的原理差异。 
    边缘检测的基本原理：1. 边缘检测的本质是微分。 2. 实际中常用差分，x方向和y方向
    Sobel、LoG和Canny算子的原理差异：
    Sobel：Sobel算子是两个Prewitte模板中心像素的权重取2倍的值。是由向量方式确定边缘的两个mask组成的。Sobel算子的这个通用形式缩合了一条坐标轴上最优平滑和另一条坐标轴上的最优差分。换而言之，Sobel算子有两个，一个是检测水平边缘的 ；另一个是检测垂直边缘的 。它对于象素的位置的影响做了加权，可以降低边缘模糊程度，因此效果更好。
    LoG：Laplacian of Gaussian计算可以利用高斯差分来近似，其中差分是由两个高斯滤波与不同变量的卷积结果求解获得。从两个平滑算子的差分得出的是二阶边缘检测。
    Canny：Canny边缘算子由三个主要目标形成：第一、无附加响应的最优检测，即不失去重要的边缘，不应有虚假的边缘；第二、实际边缘与检测到的边缘位置之间的偏差最小；第三、减少单边缘的多重响应而得到单响应。这一点被第一个目标是减少噪声响应。第二个目标是正确性，即要在正确位置检测到边缘。第三个目标限制的是单个边缘点结于亮度变化定位。Canny指出高斯算子对图像平滑处理是最优的。
    参考文档：
    [边缘检测算法综述](https://blog.csdn.net/tercel_zhang/article/details/79538317)

3. 简述图像直方图的基本概念，及使用大津算法进行图像分割的基本原理。 
    图像直方图：图像直方图(Image Histogram)是用以表示数字图像中亮度分布的直方图，标绘了图像中每个亮度值的像素数，通过灰度直方图看到图像的照明效果。
    大津算法进行图像分割的基本原理：最大类间方差是一种自适应的阈值确定方法。算法假设图像像素能够根据阈值，被分成背景[background]和目标[objects]两部分。然后，计算该最佳阈值来区分这两类像素，使得两类像素区分度最大。
    具体实现：1. 确定最佳阈值，使背景和目标之间的类间方差最大 (因为二者差异最大)。 2. 算法实现：遍历灰度取值
    参考文档：
    [详细及易读懂的 大津法（OTSU）原理 和 算法实现](https://blog.csdn.net/u012198575/article/details/81128799)
    [OTSU-method 大津算法](https://blog.csdn.net/guojunxiu/article/details/84075252)

4. 简述Harris算子对角点的定义，进行角点检测的基本原理，并说明引入角点响应函数的意义。 
    Harris算子对角点的定义：在做图像匹配时，常需要对两幅图像中的特征点进行匹配。为了保证匹配的准确性，所选择的特征必须有其独特性，角点可以作为一种不错的特征。
    角点检测的基本原理：角点原理来源于人对角点的感性判断，即图像在各个方向灰度有明显变化。算法的核心是利用局部窗口在图像上进行移动判断灰度发生较大的变化，所以此窗口用于计算图像的灰度变化为：[-1,0,1;-1,0,1;-1,0,1][-1,-1,-1;0,0,0;1,1,1]。人各个方向上移动这个特征的小窗口，如图3中窗口内区域的灰度发生了较大的变化，那么就认为在窗口内遇到了角点。如图1中，窗口内图像的灰度没有发生变化，那么窗口内就不存在角点；如果窗口在某一个方向移动时，窗口内图像的灰度发生了较大的变化，而在另一些方向上没有发生变化，那么，窗口内的图像可能就是一条直线的线段。
    ![](_v_images/20190811234547521_10957.png)
    引入角点响应函数的意义：使用角点响应函数：
    ![](_v_images/20190811234729025_8301.png)
        当R接近于零时，处于灰度变化平缓区域；
        当R<0时，点为边界像素；
        当R>0时，点为角点。

    参考文档：
    [Harris Corner（Harris角检测）](https://www.cnblogs.com/klitech/p/5779600.html)
    [Harris角点算法](https://www.cnblogs.com/polly333/p/5416172.html)

5. 简述Hough变换的基本原理(包括参数空间变换及参数空间划分网格统计)。 
    Hough变换的基本原理：Hough变换于1962年由Paul Hough提出，是一种使用表决方式的参数估计技术，其原理是利用图像空间和Hough参数空间的线-点对偶性，把图像空间中的检测问题转换到参数空间中进行。
    参数空间变换：一条直线在直角坐标系下可以用y=kx+b表示, 霍夫变换的主要思想是将该方程的参数和变量交换，即用x,y作为已知量k,b作为变量坐标，所以直角坐标系下的直线y=kx+b在参数空间表示为点(k,b)，而一个点(x1,y1)在直角坐标系下表示为一条直线y1=x1·k+b，其中(k,b)是该直线上的任意点。为了计算方便，我们将参数空间的坐标表示为极坐标下的γ和θ。因为同一条直线上的点对应的(γ,θ)是相同的，因此可以先将图片进行边缘检测，然后对图像上每一个非零像素点，在参数坐标下变换为一条直线，那么在直角坐标下属于同一条直线的点便在参数空间形成多条直线并内交于一点。因此可用该原理进行直线检测。
    参数空间划分网格统计：将空间量化成许多小格，根据x-y平面每一个直线点代入θ的量化值，算出各个ρ，将对应格计数累加。当全部点变换后，对小格进行检验。设置累计阈值T，计数器大于T的小格对应于共线点，其可以用作直线拟合参数。小于T的反映非共线点，丢弃不用
    参考文档：
    [经典霍夫变换（Hough Transform）](https://blog.csdn.net/yuyuntan/article/details/80141392)
    [Hough变换](https://blog.csdn.net/qq_15971883/article/details/80583364)
    
6. 简述SIFT原理(重点是尺度空间和方向直方图原理)及ORB算子原理(重点是FAST和BRIEF)。 
    SIFT原理：尺度不变特征转换(Scale-invariant feature transform或SIFT)是一种电脑视觉的算法用来侦测与描述影像中的局部性特征，它在空间尺度中寻找极值点，并提取出其位置、尺度、旋转不变量。其应用范围包含物体辨识、机器人地图感知与导航、影像缝合、3D模型建立、手势辨识、影像追踪和动作比对。
    SIFT算法分解为如下四步：
    1. 尺度空间极值检测：搜索所有尺度上的图像位置。通过高斯微分函数来识别潜在的对于尺度和旋转不变的兴趣点。
    2. 关键点定位：在每个候选的位置上，通过一个拟合精细的模型来确定位置和尺度。关键点的选择依据于它们的稳定程度。
    3. 方向确定：基于图像局部的梯度方向，分配给每个关键点位置一个或多个方向。所有后面的对图像数据的操作都相对于关键点的方向、尺度和位置进行变换，从而提供对于这些变换的不变性。
    4. 关键点描述：在每个关键点周围的邻域内，在选定的尺度上测量图像局部的梯度。这些梯度被变换成一种表示，这种表示允许比较大的局部形状的变形和光照变化。
    尺度空间理论的基本思想是：在图像信息处理模型中引入一个被视为尺度的参数，通过连续变化尺度参数获得多尺度下的尺度空间表示序列，对这些序列进行尺度空间主轮廓的提取，并以该主轮廓作为一种特征向量，实现边缘、角点检测和不同分辨率上的特征提取等。
    为了使描述符具有旋转不变性，需要利用图像的局部特征为给每一个关键点分配一个基准方向。使用图像梯度的方法求取局部结构的稳定方向。方向直方图的峰值则代表了该特征点处邻域梯度的方向，以直方图中最大值作为该关键点的主方向。为了增强匹配的鲁棒性，只保留峰值大于主方向峰值80％的方向作为该关键点的辅方向。因此，对于同一梯度值的多个峰值的关键点位置，在相同位置和尺度将会有多个关键点被创建但方向不同。仅有15％的关键点被赋予多个方向，但可以明显的提高关键点匹配的稳定性。实际编程实现中，就是把该关键点复制成多份关键点，并将方向值分别赋给这些复制后的关键点，并且，离散的梯度方向直方图要进行插值拟合处理，来求得更精确的方向角度值
    
    ORB算子原理：
    RB特征描述算法的运行时间远优于SIFT与SURF，可用于实时性特征检测。ORB特征基于FAST角点的特征点检测与描述技术，该算法是Ethan Rubleed 在论文“ORB:An efficient alternative to SIFT or SURF”中提出的。ORB特征具有尺度与旋转不变性，同时对噪声及透视仿射也具有不变性，良好的性能使得得用ORB在进行特征描述时的应用场景十分广泛。
        ORB特征检测主要分为以下两个步骤。
        步骤①方向FAST特征点检测
        FAST角点检测是一种基于机器学习的快速角点特征检测算法，具有方向的FAST特征点检测是对兴趣点所在圆周上的16个像素点进行判断，若判断后的当前中心像素点为暗或亮，将瘊定其是否为角点。前面我已经写了一篇博文（moravec、harris、Shi-Tomasi角点检测的简介及OpenCV代码实现）介绍角点检测的常用方法，FAST角点检测相对于这些方法来说，计算的时间复杂度小，检测效果突出。FAST角点检测为加速算法实现，通常先对回周上的点集进行排序，排序使得其计算过程大大得到了优化。FAST对多尺度特性的描述是还是通过建立图像金字塔实现的，而对于旋转不变性即方向的特征则引入灰度质心法用于描述特征点的方向。
        步骤②BRIEF特征描述
        BRIEF描述子主要是通过随机选取兴趣点周围区域的若干点来组成小兴趣区域，将这些小兴趣区域的灰度二值化并解析成二进制码串，将串特征作为该特征点的描述子，BRIEF描述子选取关键点附近的区域并对每一位比较其强度大小，然后根据图像块中两个二进制点来判断当前关键点编码是0还是1.因为BRIEF描述子的所有编码都是二进制数的，这样就节省了计算机存储空间。

    参考文档：
    [SIFT算法详解](https://blog.csdn.net/zddblog/article/details/7521424)
    [ORB算法原理解读](https://blog.csdn.net/yang843061497/article/details/38553765/)
    [图像特征检测描述(一):SIFT、SURF、ORB、HOG、LBP特征的原理概述及OpenCV代码实现](https://blog.csdn.net/wenhao_ir/article/details/52046569)