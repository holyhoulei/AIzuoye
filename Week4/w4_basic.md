
## 第4周基础作业

1. 对连续型特征，可以用哪个函数可视化其分布？（给出你最常用的一个即可），并根据代码运行结果给出示例。（10分） 
2. 对两个连续型特征，可以用哪个函数得到这两个特征之间的相关性？根据代码运行结果，给出示例。（10分） 
3. 如果发现特征之间有较强的相关性，在选择线性回归模型时应该采取什么措施。（10分） 
4. 当采用带正则的模型以及采用随机梯度下降优化算法时，需要对输入（连续型）特征进行去量纲预处理。课程代码给出了用标准化（StandardScaler）的结果，请改成最小最大缩放（MinMaxScaler）去量纲 （10分），并重新训练最小二乘线性回归、岭回归、和Lasso模型（30分）。 
5. 代码中给出了岭回归（RidgeCV）和Lasso（LassoCV）超参数（alpha_）调优的过程，请结合两个最佳模型以及最小二乘线性回归模型的结果，给出什么场合应该用岭回归，什么场合用Lasso，什么场合用最小二乘。（30分） 

第一题： 对于连续性特征，可以使用describe方法可以特征的基本统计学特性，可以通过matplotlib、seaborn等画图可视化其分布,判断是否存在噪声数据点等，最常用的有直方图（seaborn的distplot）、散点图（matplotlib库的scatter()方法 或 seaborn库的jointplot方法）



```python
#导入常用的工具包
import numpy as np
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt

#是一个魔法函数（Magic Functions）。官方给出的定义是：IPython有一组预先定义好的所谓的魔法函数（Magic Functions），你可以通过命令行的语法形式来访问它们。
#由于 %matplotlib inline 的存在，当输入plt.plot(x,y_1)后，不必再输入 plt.show()，图像将自动显示出来
%matplotlib inline
```


```python
# 使用pandas读取数据，pandas可以使用类似sql语句的方式分析处理表格数据。
# 读取当前目录下的csv文件
df = pd.read_csv("./boston_housing.csv")

#显示头5行数据
df.head()
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>CRIM</th>
      <th>ZN</th>
      <th>INDUS</th>
      <th>CHAS</th>
      <th>NOX</th>
      <th>RM</th>
      <th>AGE</th>
      <th>DIS</th>
      <th>RAD</th>
      <th>TAX</th>
      <th>PTRATIO</th>
      <th>B</th>
      <th>LSTAT</th>
      <th>MEDV</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>0.00632</td>
      <td>18</td>
      <td>2.31</td>
      <td>0</td>
      <td>0.538</td>
      <td>6.575</td>
      <td>65.2</td>
      <td>4.0900</td>
      <td>1</td>
      <td>296</td>
      <td>15</td>
      <td>396.90</td>
      <td>4.98</td>
      <td>24.0</td>
    </tr>
    <tr>
      <th>1</th>
      <td>0.02731</td>
      <td>0</td>
      <td>7.07</td>
      <td>0</td>
      <td>0.469</td>
      <td>6.421</td>
      <td>78.9</td>
      <td>4.9671</td>
      <td>2</td>
      <td>242</td>
      <td>17</td>
      <td>396.90</td>
      <td>9.14</td>
      <td>21.6</td>
    </tr>
    <tr>
      <th>2</th>
      <td>0.02729</td>
      <td>0</td>
      <td>7.07</td>
      <td>0</td>
      <td>0.469</td>
      <td>7.185</td>
      <td>61.1</td>
      <td>4.9671</td>
      <td>2</td>
      <td>242</td>
      <td>17</td>
      <td>392.83</td>
      <td>4.03</td>
      <td>34.7</td>
    </tr>
    <tr>
      <th>3</th>
      <td>0.03237</td>
      <td>0</td>
      <td>2.18</td>
      <td>0</td>
      <td>0.458</td>
      <td>6.998</td>
      <td>45.8</td>
      <td>6.0622</td>
      <td>3</td>
      <td>222</td>
      <td>18</td>
      <td>394.63</td>
      <td>2.94</td>
      <td>33.4</td>
    </tr>
    <tr>
      <th>4</th>
      <td>0.06905</td>
      <td>0</td>
      <td>2.18</td>
      <td>0</td>
      <td>0.458</td>
      <td>7.147</td>
      <td>54.2</td>
      <td>6.0622</td>
      <td>3</td>
      <td>222</td>
      <td>18</td>
      <td>396.90</td>
      <td>5.33</td>
      <td>36.2</td>
    </tr>
  </tbody>
</table>
</div>




```python
# 使用info函数，查看数据的整体信息，可以看数据集大小（506行、14列），各字段及各字段数据类型
# 也可以使用print(df.shape)可以打印样本数量和特征维度
# 也可以使用print(df.columns) 可以打印各列名称
df.info()
```

    <class 'pandas.core.frame.DataFrame'>
    RangeIndex: 506 entries, 0 to 505
    Data columns (total 14 columns):
    CRIM       506 non-null float64
    ZN         506 non-null int64
    INDUS      506 non-null float64
    CHAS       506 non-null int64
    NOX        506 non-null float64
    RM         506 non-null float64
    AGE        506 non-null float64
    DIS        506 non-null float64
    RAD        506 non-null int64
    TAX        506 non-null int64
    PTRATIO    506 non-null int64
    B          506 non-null float64
    LSTAT      506 non-null float64
    MEDV       506 non-null float64
    dtypes: float64(9), int64(5)
    memory usage: 55.4 KB



```python
# 使用describe()函数，可以查看连续性特征（数值型特征）中每个特征的
# 此处得到各属性的样本数目、均值、标准差、最小值、1/4分位数（25%）、中位数（50%）、3/4分位数（75%）、最大值 可初步了解各特征的分布
df.describe()
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>CRIM</th>
      <th>ZN</th>
      <th>INDUS</th>
      <th>CHAS</th>
      <th>NOX</th>
      <th>RM</th>
      <th>AGE</th>
      <th>DIS</th>
      <th>RAD</th>
      <th>TAX</th>
      <th>PTRATIO</th>
      <th>B</th>
      <th>LSTAT</th>
      <th>MEDV</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>count</th>
      <td>506.000000</td>
      <td>506.000000</td>
      <td>506.000000</td>
      <td>506.000000</td>
      <td>506.000000</td>
      <td>506.000000</td>
      <td>506.000000</td>
      <td>506.000000</td>
      <td>506.000000</td>
      <td>506.000000</td>
      <td>506.000000</td>
      <td>506.000000</td>
      <td>506.000000</td>
      <td>506.000000</td>
    </tr>
    <tr>
      <th>mean</th>
      <td>3.613524</td>
      <td>11.347826</td>
      <td>11.136779</td>
      <td>0.069170</td>
      <td>0.554695</td>
      <td>6.284634</td>
      <td>68.574901</td>
      <td>3.795043</td>
      <td>9.549407</td>
      <td>408.237154</td>
      <td>18.083004</td>
      <td>356.674032</td>
      <td>12.653063</td>
      <td>22.532806</td>
    </tr>
    <tr>
      <th>std</th>
      <td>8.601545</td>
      <td>23.310593</td>
      <td>6.860353</td>
      <td>0.253994</td>
      <td>0.115878</td>
      <td>0.702617</td>
      <td>28.148861</td>
      <td>2.105710</td>
      <td>8.707259</td>
      <td>168.537116</td>
      <td>2.280574</td>
      <td>91.294864</td>
      <td>7.141062</td>
      <td>9.197104</td>
    </tr>
    <tr>
      <th>min</th>
      <td>0.006320</td>
      <td>0.000000</td>
      <td>0.460000</td>
      <td>0.000000</td>
      <td>0.385000</td>
      <td>3.561000</td>
      <td>2.900000</td>
      <td>1.129600</td>
      <td>1.000000</td>
      <td>187.000000</td>
      <td>12.000000</td>
      <td>0.320000</td>
      <td>1.730000</td>
      <td>5.000000</td>
    </tr>
    <tr>
      <th>25%</th>
      <td>0.082045</td>
      <td>0.000000</td>
      <td>5.190000</td>
      <td>0.000000</td>
      <td>0.449000</td>
      <td>5.885500</td>
      <td>45.025000</td>
      <td>2.100175</td>
      <td>4.000000</td>
      <td>279.000000</td>
      <td>17.000000</td>
      <td>375.377500</td>
      <td>6.950000</td>
      <td>17.025000</td>
    </tr>
    <tr>
      <th>50%</th>
      <td>0.256510</td>
      <td>0.000000</td>
      <td>9.690000</td>
      <td>0.000000</td>
      <td>0.538000</td>
      <td>6.208500</td>
      <td>77.500000</td>
      <td>3.207450</td>
      <td>5.000000</td>
      <td>330.000000</td>
      <td>19.000000</td>
      <td>391.440000</td>
      <td>11.360000</td>
      <td>21.200000</td>
    </tr>
    <tr>
      <th>75%</th>
      <td>3.677082</td>
      <td>12.000000</td>
      <td>18.100000</td>
      <td>0.000000</td>
      <td>0.624000</td>
      <td>6.623500</td>
      <td>94.075000</td>
      <td>5.188425</td>
      <td>24.000000</td>
      <td>666.000000</td>
      <td>20.000000</td>
      <td>396.225000</td>
      <td>16.955000</td>
      <td>25.000000</td>
    </tr>
    <tr>
      <th>max</th>
      <td>88.976200</td>
      <td>100.000000</td>
      <td>27.740000</td>
      <td>1.000000</td>
      <td>0.871000</td>
      <td>8.780000</td>
      <td>100.000000</td>
      <td>12.126500</td>
      <td>24.000000</td>
      <td>711.000000</td>
      <td>22.000000</td>
      <td>396.900000</td>
      <td>37.970000</td>
      <td>50.000000</td>
    </tr>
  </tbody>
</table>
</div>




```python
# 连续性数据使用seaborn的distplot方法画直方图，查看数据分布，离散型数据使用sns.countplot查看数据分布
fig = plt.figure()
sns.distplot(df["MEDV"],bins=30,kde=True)
plt.xlabel("Median value of owner-occupied homes", fontsize=12)
plt.show()
```


![png](output_5_0.png)


从上面的直方图可以看出，图形接近于正态分布，数据主要集中在20左右，左侧比较密集，右侧比较分散，当值为50时，样本数增加，可以判断为噪声将其去除。
对于价格类数值，人类的感受复核log变换（当价格越高，敏感度越低），log变换可以使用numpy的log1p函数


```python
# 连续型数值，也可以使用散点图查看数据分布方法有两个：matplotlib库的scatter()方法 或 seaborn库的jointplot方法（kind参数为scatter）
#使用matplotlib的scatter方法，画出散点图如下：
plt.scatter(df["RM"],df["MEDV"]) #分别为x轴和y轴
```




    <matplotlib.collections.PathCollection at 0x7f1e9c75aa58>




![png](output_7_1.png)



```python
# 使用seaborn的jointplot方法话散点图如下：
sns.jointplot(x="RM",y="MEDV",data=df,kind="scatter")
```




    <seaborn.axisgrid.JointGrid at 0x7f1e9c7221d0>




![png](output_8_1.png)


从上面的散点图可以看出，数据分布具有线性化特征，但是在MEDV为50处，有部分噪声

第二题：对于两个连续型特征，主要有两种方法判断其相关性：相关矩阵和散点图，散点图的方式如上图所示，相关矩阵代码如下：


```python
#使用相关矩阵判断两个连续型特征相关性
data_corr = df.corr()
# 相关系数有正有负，我们对相关系数取绝对值，通常认为相关系数的绝对值大于0.5的特征为强相关
data_corr = data_corr.abs()
plt.subplots(figsize=(13, 9)) # subplots对应的参数还没搞太清楚，需要下来继续学习
sns.heatmap(data_corr,annot=True)
# Mask unimportant features,突出重要信息
sns.heatmap(data_corr, mask=data_corr < 0.5, cbar=False)

#plt.savefig("house_coor.png" )
plt.show()
```


![png](output_10_0.png)


第三题：当发现特征直接有较强相关性后，在选择线性回归模型时，要先对数据做特征化工程。
对原始特征做必要的数据预处理和特征编码，使得变换后的特征符合模型要求。
特征工程主要包括：
1. 数据去噪（如：MEDV中等于50的噪声需要去除）；
2. 数据分离（从原始数据中分离出特征x和标签y）；
3. 特征编码（离散型数据需要进行独热编码（one-hot encode），通过pandas的get_dummies方法（哑编码）或者Scikit-Learn中的OneHotEncoder类，将原来有K种取值的离散型特征变成K维0-1编码特征）；
4. 数值型特征预处理（在数值型特征中，如果发现各特征差异较大，需要进行数据标准化预处理，避免原始特征值差异过大，导致训练得到的参数权重单位不一致，无法比较各特征的重要性，同时一些优化算法（如随机梯度下降及其改进版本）只在各特征尺度差不多的情况下才能保证收敛）。

第四题：在数据特征标准化（去量纲）预处理中，常用的是StandardScaler，针对稀疏结果，也可以使用MinMaxScaler去量纲。下面是做完数据可视化之和，进行特征工程的详细步骤：


```python
'''
特征工程
'''

###########################################
#              1.数据去噪                  #
###########################################
# 删除y大于等于50的样本（保留小于50的样本）
df = df[df.MEDV < 50]

#输出样本数和特征维数
print(df.shape)


```

    (490, 14)



```python
###########################################
#              2.数据分离                  #
###########################################


# 从原始数据中分离输入特征x和输出y
y = df['MEDV']
X = df.drop('MEDV', axis = 1)

# 尝试对y（房屋价格）做log变换，对log变换后的价格进行估计
log_y = np.log1p(y)

```


```python
###########################################
#              3.离散型特征编码             #
###########################################
# RAD的含义是距离高速公路的便利指数。虽然给的数值是数值型，但实际是索引，可换成离散特征/类别型特征编码试试。
X["RAD"].astype("object")
X_cat = X["RAD"]
X_cat = pd.get_dummies(X_cat, prefix="RAD")

X = X.drop("RAD", axis = 1)

#特征名称，用于保存特征工程结果
feat_names = X.columns

X_cat.head()
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>RAD_1</th>
      <th>RAD_2</th>
      <th>RAD_3</th>
      <th>RAD_4</th>
      <th>RAD_5</th>
      <th>RAD_6</th>
      <th>RAD_7</th>
      <th>RAD_8</th>
      <th>RAD_24</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>1</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
    </tr>
    <tr>
      <th>1</th>
      <td>0</td>
      <td>1</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
    </tr>
    <tr>
      <th>2</th>
      <td>0</td>
      <td>1</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
    </tr>
    <tr>
      <th>3</th>
      <td>0</td>
      <td>0</td>
      <td>1</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
    </tr>
    <tr>
      <th>4</th>
      <td>0</td>
      <td>0</td>
      <td>1</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
    </tr>
  </tbody>
</table>
</div>




```python
###########################################
#              4.数值型特征标准化           #
###########################################
#使用MinMaxScaler去量纲，导入函数
from sklearn.preprocessing import MinMaxScaler 

#分别初始化特征值x和目标值y的标准化对象
mms_X = MinMaxScaler()
mms_y = MinMaxScaler()

mms_log_y = MinMaxScaler()

# 分别对训练和测试数据的特征以及目标值进行标准化处理
# 对训练数据，要先调用fit方法训练模型，得到模型参数；然后对训练数据和测试数据进行transform
X = mms_X.fit_transform(X)

# 对y做标准化不是必须的
# 对y做标准化的好处是不同问题的w差异不太大，同时正则参数的范围也有限

#对y做标准化不是必须
#对y标准化的好处是不同问题的w差异不太大，同时正则参数的范围也有限
y = mms_y.fit_transform(y.reshape(-1, 1))
log_y = mms_y.fit_transform(log_y.reshape(-1, 1))
```


```python
'''
保存特征工程的结果到文件，供机器学习模型使用
'''
fe_mms_data = pd.DataFrame(data=X, columns=feat_names, index=df.index)
fe_mms_data = pd.concat([fe_mms_data, X_cat], axis=1, ignore_index=False)

#加上标签y
fe_mms_data["MEDV"] = y
fe_mms_data["log_MEDV"] = log_y

#保存结果到文件
fe_mms_data.to_csv('FE_mms_boston_housing.csv', index=False)

#显示头5行
fe_mms_data.head()
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>CRIM</th>
      <th>ZN</th>
      <th>INDUS</th>
      <th>CHAS</th>
      <th>NOX</th>
      <th>RM</th>
      <th>AGE</th>
      <th>DIS</th>
      <th>TAX</th>
      <th>PTRATIO</th>
      <th>...</th>
      <th>RAD_2</th>
      <th>RAD_3</th>
      <th>RAD_4</th>
      <th>RAD_5</th>
      <th>RAD_6</th>
      <th>RAD_7</th>
      <th>RAD_8</th>
      <th>RAD_24</th>
      <th>MEDV</th>
      <th>log_MEDV</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>0.000000</td>
      <td>0.18</td>
      <td>0.058148</td>
      <td>0.0</td>
      <td>0.314815</td>
      <td>0.577505</td>
      <td>0.641607</td>
      <td>0.268711</td>
      <td>0.208015</td>
      <td>0.3</td>
      <td>...</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0.433790</td>
      <td>0.674359</td>
    </tr>
    <tr>
      <th>1</th>
      <td>0.000236</td>
      <td>0.00</td>
      <td>0.234444</td>
      <td>0.0</td>
      <td>0.172840</td>
      <td>0.547998</td>
      <td>0.782698</td>
      <td>0.348524</td>
      <td>0.104962</td>
      <td>0.5</td>
      <td>...</td>
      <td>1</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0.378995</td>
      <td>0.626668</td>
    </tr>
    <tr>
      <th>2</th>
      <td>0.000236</td>
      <td>0.00</td>
      <td>0.234444</td>
      <td>0.0</td>
      <td>0.172840</td>
      <td>0.694386</td>
      <td>0.599382</td>
      <td>0.348524</td>
      <td>0.104962</td>
      <td>0.5</td>
      <td>...</td>
      <td>1</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0.678082</td>
      <td>0.842711</td>
    </tr>
    <tr>
      <th>3</th>
      <td>0.000293</td>
      <td>0.00</td>
      <td>0.053333</td>
      <td>0.0</td>
      <td>0.150206</td>
      <td>0.658555</td>
      <td>0.441813</td>
      <td>0.448173</td>
      <td>0.066794</td>
      <td>0.6</td>
      <td>...</td>
      <td>0</td>
      <td>1</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0.648402</td>
      <td>0.825183</td>
    </tr>
    <tr>
      <th>4</th>
      <td>0.000705</td>
      <td>0.00</td>
      <td>0.053333</td>
      <td>0.0</td>
      <td>0.150206</td>
      <td>0.687105</td>
      <td>0.528321</td>
      <td>0.448173</td>
      <td>0.066794</td>
      <td>0.6</td>
      <td>...</td>
      <td>0</td>
      <td>1</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0.712329</td>
      <td>0.862159</td>
    </tr>
  </tbody>
</table>
<p>5 rows × 23 columns</p>
</div>




```python
# 显示标准化后数据统计信息
fe_mms_data.info()
```

    <class 'pandas.core.frame.DataFrame'>
    Int64Index: 490 entries, 0 to 505
    Data columns (total 23 columns):
    CRIM        490 non-null float64
    ZN          490 non-null float64
    INDUS       490 non-null float64
    CHAS        490 non-null float64
    NOX         490 non-null float64
    RM          490 non-null float64
    AGE         490 non-null float64
    DIS         490 non-null float64
    TAX         490 non-null float64
    PTRATIO     490 non-null float64
    B           490 non-null float64
    LSTAT       490 non-null float64
    RAD_1       490 non-null uint8
    RAD_2       490 non-null uint8
    RAD_3       490 non-null uint8
    RAD_4       490 non-null uint8
    RAD_5       490 non-null uint8
    RAD_6       490 non-null uint8
    RAD_7       490 non-null uint8
    RAD_8       490 non-null uint8
    RAD_24      490 non-null uint8
    MEDV        490 non-null float64
    log_MEDV    490 non-null float64
    dtypes: float64(14), uint8(9)
    memory usage: 61.7 KB



```python
'''
重新准备数据
'''
#重新打开标准化后的数据文件
df = pd.read_csv("FE_mms_boston_housing.csv")
#从原始数据中分离出特征X和输出y
y = df["MEDV"]
X = df.drop(["MEDV","log_MEDV"],axis = 1)

#特征名称，用于后续显示权重系数对应的特征
feat_names = X.columns

'''
采用交叉验证，将数据分割训练数据与测试数据
'''
#导入train_test_split
from sklearn.model_selection import train_test_split

# 随机采样20%的数据构建测试样本，其余作为训练样本
X_train, X_test, y_train, y_test = train_test_split(X, y, random_state=33, test_size=0.2)
```


```python
###########################################
#              训练最小二乘回归             #
###########################################
from sklearn.linear_model import LinearRegression

#初始化机器学习实例
lr = LinearRegression()

#用训练数据训练模型参数
lr.fit(X_train, y_train)

#用训练好的模型对测试集进行预测
y_test_pred_lr = lr.predict(X_test)
y_train_pred_lr = lr.predict(X_train)

#查看训练后求解出的各特征的权重系数，系数的绝对值大学可视为该特征的重要性
fs = pd.DataFrame({"columns":list(feat_names),"coef":list(lr.coef_.T)})
fs.sort_values(by=['coef'],ascending=False)

```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>columns</th>
      <th>coef</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>5</th>
      <td>RM</td>
      <td>0.446699</td>
    </tr>
    <tr>
      <th>1</th>
      <td>ZN</td>
      <td>0.104658</td>
    </tr>
    <tr>
      <th>20</th>
      <td>RAD_24</td>
      <td>0.061897</td>
    </tr>
    <tr>
      <th>10</th>
      <td>B</td>
      <td>0.041508</td>
    </tr>
    <tr>
      <th>19</th>
      <td>RAD_8</td>
      <td>0.039532</td>
    </tr>
    <tr>
      <th>18</th>
      <td>RAD_7</td>
      <td>0.033091</td>
    </tr>
    <tr>
      <th>14</th>
      <td>RAD_3</td>
      <td>0.021226</td>
    </tr>
    <tr>
      <th>3</th>
      <td>CHAS</td>
      <td>0.008079</td>
    </tr>
    <tr>
      <th>16</th>
      <td>RAD_5</td>
      <td>-0.011200</td>
    </tr>
    <tr>
      <th>15</th>
      <td>RAD_4</td>
      <td>-0.013261</td>
    </tr>
    <tr>
      <th>13</th>
      <td>RAD_2</td>
      <td>-0.020636</td>
    </tr>
    <tr>
      <th>2</th>
      <td>INDUS</td>
      <td>-0.024599</td>
    </tr>
    <tr>
      <th>6</th>
      <td>AGE</td>
      <td>-0.041786</td>
    </tr>
    <tr>
      <th>12</th>
      <td>RAD_1</td>
      <td>-0.054486</td>
    </tr>
    <tr>
      <th>17</th>
      <td>RAD_6</td>
      <td>-0.056163</td>
    </tr>
    <tr>
      <th>8</th>
      <td>TAX</td>
      <td>-0.109003</td>
    </tr>
    <tr>
      <th>4</th>
      <td>NOX</td>
      <td>-0.114821</td>
    </tr>
    <tr>
      <th>9</th>
      <td>PTRATIO</td>
      <td>-0.180497</td>
    </tr>
    <tr>
      <th>0</th>
      <td>CRIM</td>
      <td>-0.191897</td>
    </tr>
    <tr>
      <th>7</th>
      <td>DIS</td>
      <td>-0.331288</td>
    </tr>
    <tr>
      <th>11</th>
      <td>LSTAT</td>
      <td>-0.345060</td>
    </tr>
  </tbody>
</table>
</div>




```python
'''
模型评价
'''
from sklearn.metrics import r2_score  #评价回归预测模型的性能

# 使用r2_score评价模型在测试集和训练集上的性能，并输出评估结果
#测试集
print(f'The r2 score of LinearRegression on test is {r2_score(y_test, y_test_pred_lr)}')
#训练集
print(f'The r2 score of LinearRegression on train is {r2_score(y_train, y_train_pred_lr)}')
```

    The r2 score of LinearRegression on test is 0.691834670320638
    The r2 score of LinearRegression on train is 0.8001507257439993


从上面r2 score可以看出，最小二乘回归存在过拟合的情况


```python
#在训练集上观察预测残差的分布，看是否符合模型假设：噪声为0均值的高斯噪声
f, ax = plt.subplots(figsize=(7, 5)) 
f.tight_layout() 
ax.hist(y_train - y_train_pred_lr, bins=40, label='Residuals Linear', color='b', alpha=.5); 
ax.set_title("Histogram of Residuals") 
ax.legend(loc='best');
```


![png](output_23_0.png)


从上图看出，残差分布和高斯分布比较匹配，但还是大值这边的尾巴更长


```python
#还可以观察预测值与真值的散点图
plt.figure(figsize=(4, 3))
plt.scatter(y_train, y_train_pred_lr)
plt.plot([-3, 3], [-3, 3], '--k')   #数据已经标准化，3倍标准差即可
plt.axis('tight')
plt.xlabel('True price')
plt.ylabel('Predicted price')
plt.tight_layout()
```


![png](output_25_0.png)


从上图可以看出，使用MinMaxScaler标准化，预测结果好于StandardScaler，样本数据中，存在较多稀疏数据


```python
###########################################
#              使用岭回归训练               #
###########################################
#岭回归／L2正则
#class sklearn.linear_model.RidgeCV(alphas=(0.1, 1.0, 10.0), fit_intercept=True, 
#                                  normalize=False, scoring=None, cv=None, gcv_mode=None, 
#                                  store_cv_values=False)
from sklearn.linear_model import  RidgeCV

#1. 设置超参数（正则参数）范围
alphas = [ 0.01, 0.1, 1, 10,100]
#n_alphas = 20
#alphas = np.logspace(-5,2,n_alphas)

#2. 生成一个RidgeCV实例
ridge = RidgeCV(alphas=alphas, store_cv_values=True)  

#3. 模型训练
ridge.fit(X_train, y_train)    

#4. 预测
y_test_pred_ridge = ridge.predict(X_test)
y_train_pred_ridge = ridge.predict(X_train)


# 评估，使用r2_score评价模型在测试集和训练集上的性能
print(f'The r2 score of RidgeCV on test is {r2_score(y_test, y_test_pred_ridge)}')
print(f'The r2 score of RidgeCV on train is {r2_score(y_train, y_train_pred_ridge)}')
```

    The r2 score of RidgeCV on test is 0.6932533010598585
    The r2 score of RidgeCV on train is 0.8000790186112845



```python
mse_mean = np.mean(ridge.cv_values_, axis = 0)
plt.plot(np.log10(alphas), mse_mean.reshape(len(alphas),1)) 

#这是为了标出最佳参数的位置，不是必须
#plt.plot(np.log10(ridge.alpha_)*np.ones(3), [0.28, 0.29, 0.30])

plt.xlabel('log(alpha)')
plt.ylabel('mse')
plt.show()

print ('alpha is:', ridge.alpha_)
```


![png](output_28_0.png)


    alpha is: 0.1



```python
# 看看各特征的权重系数，系数的绝对值大小可视为该特征的重要性
fs = pd.DataFrame({"columns":list(feat_names), "coef_lr":list((lr.coef_.T)), "coef_ridge":list((ridge.coef_.T))})
fs.sort_values(by=['coef_lr'],ascending=False)
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>columns</th>
      <th>coef_lr</th>
      <th>coef_ridge</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>5</th>
      <td>RM</td>
      <td>0.446699</td>
      <td>0.440948</td>
    </tr>
    <tr>
      <th>1</th>
      <td>ZN</td>
      <td>0.104658</td>
      <td>0.101971</td>
    </tr>
    <tr>
      <th>20</th>
      <td>RAD_24</td>
      <td>0.061897</td>
      <td>0.058989</td>
    </tr>
    <tr>
      <th>10</th>
      <td>B</td>
      <td>0.041508</td>
      <td>0.041514</td>
    </tr>
    <tr>
      <th>19</th>
      <td>RAD_8</td>
      <td>0.039532</td>
      <td>0.039682</td>
    </tr>
    <tr>
      <th>18</th>
      <td>RAD_7</td>
      <td>0.033091</td>
      <td>0.032474</td>
    </tr>
    <tr>
      <th>14</th>
      <td>RAD_3</td>
      <td>0.021226</td>
      <td>0.021768</td>
    </tr>
    <tr>
      <th>3</th>
      <td>CHAS</td>
      <td>0.008079</td>
      <td>0.008555</td>
    </tr>
    <tr>
      <th>16</th>
      <td>RAD_5</td>
      <td>-0.011200</td>
      <td>-0.010714</td>
    </tr>
    <tr>
      <th>15</th>
      <td>RAD_4</td>
      <td>-0.013261</td>
      <td>-0.013318</td>
    </tr>
    <tr>
      <th>13</th>
      <td>RAD_2</td>
      <td>-0.020636</td>
      <td>-0.018895</td>
    </tr>
    <tr>
      <th>2</th>
      <td>INDUS</td>
      <td>-0.024599</td>
      <td>-0.025797</td>
    </tr>
    <tr>
      <th>6</th>
      <td>AGE</td>
      <td>-0.041786</td>
      <td>-0.041070</td>
    </tr>
    <tr>
      <th>12</th>
      <td>RAD_1</td>
      <td>-0.054486</td>
      <td>-0.054392</td>
    </tr>
    <tr>
      <th>17</th>
      <td>RAD_6</td>
      <td>-0.056163</td>
      <td>-0.055594</td>
    </tr>
    <tr>
      <th>8</th>
      <td>TAX</td>
      <td>-0.109003</td>
      <td>-0.106944</td>
    </tr>
    <tr>
      <th>4</th>
      <td>NOX</td>
      <td>-0.114821</td>
      <td>-0.110663</td>
    </tr>
    <tr>
      <th>9</th>
      <td>PTRATIO</td>
      <td>-0.180497</td>
      <td>-0.179602</td>
    </tr>
    <tr>
      <th>0</th>
      <td>CRIM</td>
      <td>-0.191897</td>
      <td>-0.178385</td>
    </tr>
    <tr>
      <th>7</th>
      <td>DIS</td>
      <td>-0.331288</td>
      <td>-0.321298</td>
    </tr>
    <tr>
      <th>11</th>
      <td>LSTAT</td>
      <td>-0.345060</td>
      <td>-0.345639</td>
    </tr>
  </tbody>
</table>
</div>




```python
###########################################
#              使用Lasso训练               #
###########################################
#### Lasso／L1正则
# class sklearn.linear_model.LassoCV(eps=0.001, n_alphas=100, alphas=None, fit_intercept=True, 
#                                    normalize=False, precompute=’auto’, max_iter=1000, 
#                                    tol=0.0001, copy_X=True, cv=None, verbose=False, n_jobs=1,
#                                    positive=False, random_state=None, selection=’cyclic’)
from sklearn.linear_model import LassoCV

#1. 设置超参数搜索范围
#alphas = [ 0.01, 0.1, 1, 10,100]

#2. 生成学习器实例
#lasso = LassoCV(alphas=alphas)

#1. 设置超参数搜索范围
#Lasso可以自动确定最大的alpha，所以另一种设置alpha的方式是设置最小的alpha值（eps） 和 超参数的数目（n_alphas），
#然后LassoCV对最小值和最大值之间在log域上均匀取值n_alphas个
# np.logspace(np.log10(alpha_max * eps), np.log10(alpha_max),num=n_alphas)[::-1]

#2 生成LassoCV实例（默认超参数搜索范围）
lasso = LassoCV()  

#3. 训练（内含CV）
lasso.fit(X_train, y_train)  

#4. 测试
y_test_pred_lasso = lasso.predict(X_test)
y_train_pred_lasso = lasso.predict(X_train)


# 评估，使用r2_score评价模型在测试集和训练集上的性能
print(f'The r2 score of LassoCV on test is {r2_score(y_test, y_test_pred_lasso)}')
print(f'The r2 score of LassoCV on train is {r2_score(y_train, y_train_pred_lasso)}')
```

    The r2 score of LassoCV on test is 0.6472444626686267
    The r2 score of LassoCV on train is 0.7907259003564621


    /home/leon/anaconda3/lib/python3.7/site-packages/sklearn/model_selection/_split.py:1978: FutureWarning: The default value of cv will change from 3 to 5 in version 0.22. Specify it explicitly to silence this warning.
      warnings.warn(CV_WARNING, FutureWarning)



```python
mses = np.mean(lasso.mse_path_, axis = 1)
plt.plot(np.log10(lasso.alphas_), mses) 
#plt.plot(np.log10(lasso.alphas_)*np.ones(3), [0.3, 0.4, 1.0])
plt.xlabel('log(alpha)')
plt.ylabel('mse')
plt.show()    
            
print ('alpha is:', lasso.alpha_)
```


![png](output_31_0.png)


    alpha is: 0.0005447049278906209



```python
# 看看各特征的权重系数，系数的绝对值大小可视为该特征的重要性
fs = pd.DataFrame({"columns":list(feat_names), "coef_lr":list((lr.coef_.T)), "coef_ridge":list((ridge.coef_.T)), "coef_lasso":list((lasso.coef_.T))})
fs.sort_values(by=['coef_lr'],ascending=False)
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>columns</th>
      <th>coef_lr</th>
      <th>coef_ridge</th>
      <th>coef_lasso</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>5</th>
      <td>RM</td>
      <td>0.446699</td>
      <td>0.440948</td>
      <td>0.453778</td>
    </tr>
    <tr>
      <th>1</th>
      <td>ZN</td>
      <td>0.104658</td>
      <td>0.101971</td>
      <td>0.064915</td>
    </tr>
    <tr>
      <th>20</th>
      <td>RAD_24</td>
      <td>0.061897</td>
      <td>0.058989</td>
      <td>0.008406</td>
    </tr>
    <tr>
      <th>10</th>
      <td>B</td>
      <td>0.041508</td>
      <td>0.041514</td>
      <td>0.028353</td>
    </tr>
    <tr>
      <th>19</th>
      <td>RAD_8</td>
      <td>0.039532</td>
      <td>0.039682</td>
      <td>0.031060</td>
    </tr>
    <tr>
      <th>18</th>
      <td>RAD_7</td>
      <td>0.033091</td>
      <td>0.032474</td>
      <td>0.016817</td>
    </tr>
    <tr>
      <th>14</th>
      <td>RAD_3</td>
      <td>0.021226</td>
      <td>0.021768</td>
      <td>0.029484</td>
    </tr>
    <tr>
      <th>3</th>
      <td>CHAS</td>
      <td>0.008079</td>
      <td>0.008555</td>
      <td>0.005649</td>
    </tr>
    <tr>
      <th>16</th>
      <td>RAD_5</td>
      <td>-0.011200</td>
      <td>-0.010714</td>
      <td>-0.000000</td>
    </tr>
    <tr>
      <th>15</th>
      <td>RAD_4</td>
      <td>-0.013261</td>
      <td>-0.013318</td>
      <td>-0.007142</td>
    </tr>
    <tr>
      <th>13</th>
      <td>RAD_2</td>
      <td>-0.020636</td>
      <td>-0.018895</td>
      <td>-0.000000</td>
    </tr>
    <tr>
      <th>2</th>
      <td>INDUS</td>
      <td>-0.024599</td>
      <td>-0.025797</td>
      <td>-0.020070</td>
    </tr>
    <tr>
      <th>6</th>
      <td>AGE</td>
      <td>-0.041786</td>
      <td>-0.041070</td>
      <td>-0.026738</td>
    </tr>
    <tr>
      <th>12</th>
      <td>RAD_1</td>
      <td>-0.054486</td>
      <td>-0.054392</td>
      <td>-0.034440</td>
    </tr>
    <tr>
      <th>17</th>
      <td>RAD_6</td>
      <td>-0.056163</td>
      <td>-0.055594</td>
      <td>-0.036786</td>
    </tr>
    <tr>
      <th>8</th>
      <td>TAX</td>
      <td>-0.109003</td>
      <td>-0.106944</td>
      <td>-0.066986</td>
    </tr>
    <tr>
      <th>4</th>
      <td>NOX</td>
      <td>-0.114821</td>
      <td>-0.110663</td>
      <td>-0.068198</td>
    </tr>
    <tr>
      <th>9</th>
      <td>PTRATIO</td>
      <td>-0.180497</td>
      <td>-0.179602</td>
      <td>-0.161135</td>
    </tr>
    <tr>
      <th>0</th>
      <td>CRIM</td>
      <td>-0.191897</td>
      <td>-0.178385</td>
      <td>-0.000000</td>
    </tr>
    <tr>
      <th>7</th>
      <td>DIS</td>
      <td>-0.331288</td>
      <td>-0.321298</td>
      <td>-0.213980</td>
    </tr>
    <tr>
      <th>11</th>
      <td>LSTAT</td>
      <td>-0.345060</td>
      <td>-0.345639</td>
      <td>-0.367637</td>
    </tr>
  </tbody>
</table>
</div>



第五题：
岭回归：L2损失 + L2正则，大部分场合都适合岭回归，尤其是高相关性的情况，L2正则参数越大，收敛越快，可以有效得降低模型复杂度，其优点是处处可导，方便优化计算，但同时对于噪声也非常敏感。
Lasso：L2损失 + L1正则，岭回归与Lasso回归最大的区别在于岭回归引入的是L2正则，Lasso回归引入的是L1正则，可以减少共线性的影响，计算量Lasso回归将远远小于岭回归。
最小二乘：没有正则项，建模快速简单，特别适用于要建模的关系不是非常复杂且数据量不大的情况，但因为缺少正则项，容易产生过拟合
